// local reviews data

const contentReviews =[
  {
    id: 0,
    name: "susan smith",
    job: "web developer",
    img:
      "https://res.cloudinary.com/diqqf3eq2/image/upload/v1586883334/person-1_rfzshl.jpg",
    text:
      "I'm baby meggings twee health goth +1. Bicycle rights tumeric chartreuse before they sold out chambray pop-up. Shaman humblebrag pickled coloring book salvia hoodie, cold-pressed four dollar toast everyday carry",
  },
  {
    id: 1,
    name: "anna johnson",
    job: "web designer",
    img:
      "https://res.cloudinary.com/diqqf3eq2/image/upload/v1586883409/person-2_np9x5l.jpg",
    text:
      "Helvetica artisan kinfolk thundercats lumbersexual blue bottle. Disrupt glossier gastropub deep v vice franzen hell of brooklyn twee enamel pin fashion axe.photo booth jean shorts artisan narwhal.",
  },
  {
    id: 2,
    name: "peter jones",
    job: "intern",
    img:
      "https://res.cloudinary.com/diqqf3eq2/image/upload/v1586883417/person-3_ipa0mj.jpg",
    text:
      "Sriracha literally flexitarian irony, vape marfa unicorn. Glossier tattooed 8-bit, fixie waistcoat offal activated charcoal slow-carb marfa hell of pabst raclette post-ironic jianbing swag.",
  },
  {
    id: 3,
    name: "bill anderson",
    job: "the boss",
    img:
      "https://res.cloudinary.com/diqqf3eq2/image/upload/v1586883423/person-4_t9nxjt.jpg",
    text:
      "Edison bulb put a bird on it humblebrag, marfa pok pok heirloom fashion axe cray stumptown venmo actually seitan. VHS farm-to-table schlitz, edison bulb pop-up 3 wolf moon tote bag street art shabby chic. ",
  },
];

let personImg = document.getElementById('person-img');

let author = document.getElementById('author');

let job = document.getElementById('job');

let info = document.getElementById('info');

let prevBtn = document.querySelector('.prev-btn');

let nextBtn =document.querySelector('.next-btn');

let randomReviews = document.querySelector('.random-btn');


let currentItem = 0;


window.addEventListener("DOMContentLoaded", function (){
const item = contentReviews[currentItem];
personImg.src = item.img;
author.textContent = item.name
job.textContent = item.job
info.textContent = item.text
});

function showPerson(person) {
const item = contentReviews[person];
personImg.src = item.img;
author.textContent = item.name
job.textContent = item.job
info.textContent = item.text
};

nextBtn.addEventListener("click", function () {
  currentItem ++;
  if (currentItem > contentReviews.length -1) {
    currentItem = 0
  }
  showPerson(currentItem)
});
prevBtn.addEventListener("click", function () {
  currentItem --;
  if (currentItem < 0) {
    currentItem = contentReviews.length -1
  }
  showPerson(currentItem)
});

randomReviews.addEventListener("click", function () {
  currentItem = Math.floor(Math.random() * contentReviews.length)
  showPerson(currentItem)
})


